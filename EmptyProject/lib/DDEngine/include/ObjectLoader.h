#pragma once

#include "Object3D.h"
#include <string>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace DDEngine
{

class ObjectLoader {
	
	private:

		// Create an instance of the Importer class
		Assimp::Importer importer;

	public:

		ObjectLoader();
		~ObjectLoader();

		const aiScene* load(const std::string &path);
		const Assimp::Importer& getImporter() { return importer; }

};
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        