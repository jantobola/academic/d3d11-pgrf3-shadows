#pragma once

#include "D3DUtils.h"
#include <string>

namespace DDEngine
{
	struct Texture
	{

		enum class TextureType
		{
			DIFFUSE,
			NORMAL,
			SPECULAR,
			AMBIENT,
			EMISSIVE,
			HEIGHT,
			SHININESS,
			OPACITY,
			DISPLACEMENT,
			LIGHTMAP,
			REFLECTION,
			UNKNOWN
		};

		Texture() { }

		Texture(std::string path, ShaderResourceView* texture, TextureType type)
			: path(path), texture(texture), type(type) { }

		std::string path;
		ShaderResourceView* texture;
		TextureType type;

		static Texture::TextureType valueOf(std::string type);
	};
}