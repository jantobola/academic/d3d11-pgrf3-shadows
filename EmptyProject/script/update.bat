@echo off

xcopy "e:\GITHUB\DDEngine\DDEngine\bin\Debug\bin\*.lib" "e:\BITBUCKET\PGRF3\EmptyProject\lib\DDEngine\lib\" /s /y
xcopy "e:\GITHUB\DDEngine\DDEngine\bin\Release\bin\*.lib" "e:\BITBUCKET\PGRF3\EmptyProject\lib\DDEngine\lib\" /s /y
xcopy "e:\GITHUB\DDEngine\DDEngineRes\bin\*.dll" "e:\BITBUCKET\PGRF3\EmptyProject\lib\DDEngine\dll\Debug\" /s /y
xcopy "e:\GITHUB\DDEngine\DDEngineRes\bin\*.dll" "e:\BITBUCKET\PGRF3\EmptyProject\lib\DDEngine\dll\Release\" /s /y
xcopy "e:\GITHUB\DDEngine\DDEngine\include\*.h" "e:\BITBUCKET\PGRF3\EmptyProject\lib\DDEngine\include\" /y
xcopy "e:\GITHUB\DDEngine\DDEngine\src\*" "e:\BITBUCKET\PGRF3\EmptyProject\lib\DDEngine\src\" /s /y

echo.
echo -------------------
echo -   SUCCESSFUL    -
echo -------------------
echo.
