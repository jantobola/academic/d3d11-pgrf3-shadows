#include "CustomScene.h"
#include <D3DUtils.h>

using namespace DDEngine;
using namespace DirectX;
typedef Texture::TextureType Tex;

void CustomScene::create()
{
	initGUI();
	
	// handling of engine config and console commands

	getConfig().delegate(
		[&](const std::vector<std::string>& tokens) { handleConfig(tokens); }
	);

	getCommandExecutor().delegate(
		[&](const std::vector<std::string>& tokens) { handleCommand(tokens); }
	);

	// create constant buffers

	shaders->addConstantBuffer("cb_matrices", sizeof(Matrices));
	shaders->addConstantBuffer("cb_light", sizeof(Light));
	cb_light.attenuation = 0;
	cb_light.spotAngle = 19;
	cb_light.spotOuterAngle = 38;
	cb_light.lightDirection = DirectX::XMFLOAT3(-0.58f, 0.40f, -0.65f);
	cb_light.lightType = 2;

	// create objects

	ground = Grid(gridSizeX, gridSizeY);
	ground.addShaderCombination("GroundShader", "VS_Ground", "PS_Mesh", "POS_NOR");
	ground.registerObject("ground", Ctx);
	ground.defineMaterials(cb_light, "cb_light", 5);

	jeep = ModelObject("res/models/jeep/jeep.ms3d");
	jeep.addShaderCombination("BasicMeshShader", "VS_Mesh", "PS_Mesh", "POS_NOR_TAN_TEX");
	jeep.registerObject("jeep", Ctx);
	jeep.defineMaterials(cb_light, "cb_light", 5);

	barrel = ModelObject("res/models/barrel/barrel.3ds");
	barrel.addShaderCombination("BasicMeshShader", "VS_Mesh", "PS_Mesh", "POS_NOR_TAN_TEX");
	barrel.registerObject("barrel", Ctx);
	barrel.defineMaterials(cb_light, "cb_light", 5);

	light.addShaderCombination("BasicMeshShader", "VS_Mesh", "PS_Mesh", "POS_NOR_TAN_TEX");
	light.registerObject("light", Ctx);
	light.defineMaterials(cb_light, "cb_light", 5);
	light.translate(0, 2, 7);

	shadowMap = RenderToTexture(Ctx);
	shadowMap.create(config.CFG_SCREEN_WIDTH, config.CFG_SCREEN_HEIGHT);
	shadowMap.createDepth();

}

void CustomScene::render()
{

	// animate light

	XMMATRIX lightWorld_T = light.getWorldMatrix_T();
	cb_light.lightPosition = light.getWorldPosition();

	if (lightAnimation) {
		
		light.rotateY(timer.velocity() * 20);
		light.rotateX(timer.velocity() * 5);
		light.rotateZ(timer.velocity() * -8);

		XMFLOAT4X4 tr = light.getTranslationMatrix();
		XMFLOAT4X4 sc = light.getScaleMatrix();
		XMFLOAT4X4 ro = light.getRotationMatrix();

		XMMATRIX t = XMLoadFloat4x4(&tr);
		XMMATRIX s = XMLoadFloat4x4(&sc);
		XMMATRIX r = XMLoadFloat4x4(&ro);

		// rotate it after translation to make a circular movement
		XMMATRIX lightWorld = t * s * r;
		lightWorld_T = XMMatrixTranspose(lightWorld);
		XMVECTOR rot, sca, tra;
		XMMatrixDecompose(&sca, &rot, &tra, lightWorld);
	
		XMStoreFloat3(&cb_light.lightPosition, tra);
	}
	
	// update light (per frame)

	cb_light.eyePosition = camera.eye;
	shaders->updateConstantBufferPS("cb_light", &cb_light, 5);

#define CAMERA_TO_LIGHT XMStoreFloat4x4(&cb_matrices.view, camera.getViewMatrixFromDirection_T(cb_light.lightPosition, cb_light.lightDirection))
	
	for (size_t i = 0; i < 2; i++) {
		
		if (i == 0) {
			shadowMap.setAsRenderTarget();
			shadowMap.clearRenderTarget();
		}
		else {
			Ctx.setBackbufferRenderTarget();
			Ctx.setPSResource(shadowMap.getDepthShaderResourceView(), 2);
		}

		// draw light

		(i == 0) ? setShadowMapShader(light) : setMeshShader(light);
		resources->assignResources(light);
		light.updateMatrices(cb_matrices);
		if (i == 0) CAMERA_TO_LIGHT;
		storeDepthMatrix(light);
		XMStoreFloat4x4(&cb_matrices.world, lightWorld_T);
		shaders->updateConstantBufferVS("cb_matrices", &cb_matrices, 0);
		shaders->updateConstantBufferPS("cb_matrices", &cb_matrices, 0);
		if (i != 0) light.draw();

		Ctx.setPSSampler(Ctx.commonStates->PointClamp(), 1);

		// draw models

		(i == 0) ? setShadowMapShader(jeep) : setMeshShader(jeep);
		resources->assignResources(jeep);
		jeep.updateMatrices(cb_matrices);
		if (i == 0) CAMERA_TO_LIGHT;
		storeDepthMatrix(jeep);
		shaders->updateConstantBufferVS("cb_matrices", &cb_matrices, 0);
		shaders->updateConstantBufferPS("cb_matrices", &cb_matrices, 0);
		jeep.draw();

		(i == 0) ? setShadowMapShader(barrel) : setMeshShader(barrel);
		resources->assignResources(barrel);
		barrel.updateMatrices(cb_matrices);
		if (i == 0) CAMERA_TO_LIGHT;
		storeDepthMatrix(barrel);
		shaders->updateConstantBufferVS("cb_matrices", &cb_matrices, 0);
		shaders->updateConstantBufferPS("cb_matrices", &cb_matrices, 0);
		barrel.draw();

		// draw grid objects

		(i == 0) ? setShadowMapShader(ground) : setMeshShader(ground);
		resources->assignResources(ground);
		ground.updateMatrices(cb_matrices);
		if (i == 0) CAMERA_TO_LIGHT;
		storeDepthMatrix(ground);
		shaders->updateConstantBufferVS("cb_matrices", &cb_matrices, 0);
		shaders->updateConstantBufferPS("cb_matrices", &cb_matrices, 0);
		ground.draw();

		Ctx.setPSResource(nullptr, 2);
	}

}

void CustomScene::initGUI()
{
	switcher = TwNewBar("Switcher");

	TwDefine(" Switcher size='290 300' ");
	TwDefine(" Switcher resizable=false label='Menu' ");
	TwDefine(" Switcher valueswidth=100");
	TwDefine(" Switcher valueswidth=65");
	AntUtils::setAppearance("Switcher");

	int barPos[2] = { 10, 45 };
	TwSetParam(switcher, nullptr, "position", TW_PARAM_INT32, 2, &barPos);

	TwAddVarRW(switcher, "Animate light", TwType::TW_TYPE_BOOLCPP, &lightAnimation, "");
	
	TwAddSeparator(switcher, "separ1", "");
	
	TwAddVarRW(switcher, "Light type", TwType::TW_TYPE_INT32, &cb_light.lightType, "step=1 min=1 max=2");
	TwAddVarRW(switcher, "Light attenuation", TwType::TW_TYPE_FLOAT, &cb_light.attenuation, "step=1 min=0 max=100000");
	TwAddVarRW(switcher, "Spotlight angle", TwType::TW_TYPE_FLOAT, &cb_light.spotAngle, "step=1");
	TwAddVarRW(switcher, "Spotlight outer angle", TwType::TW_TYPE_FLOAT, &cb_light.spotOuterAngle, "step=1");
	
	TwAddSeparator(switcher, "separ2", "");
	
	TwAddVarRW(switcher, "Light direction", TwType::TW_TYPE_DIR3F, &cb_light.lightDirection, "axisx=-x axisy=-y axisz=z opened=true");

}

#include <CFG_CMD_Macros.h>

void CustomScene::handleConfig(const std::vector<std::string>& tokens)
{
	CFG_LOAD("objects.gridResolution"){
		gridSizeX = CFG_ARG_INT(0);
		gridSizeY = CFG_ARG_INT(1);
	}
}

void CustomScene::handleCommand(const std::vector<std::string>& tokens)
{

}

void CustomScene::storeDepthMatrix(DDEngine::AbstractObject& object)
{
	XMStoreFloat4x4(&cb_matrices.depthVP, XMMatrixTranspose(object.getWorldMatrix() *
		camera.getViewMatrixFromDirection(cb_light.lightPosition, cb_light.lightDirection) * 
		camera.getProjectionMatrix()));
}

void CustomScene::setShadowMapShader(DDEngine::AbstractObject& object)
{
	object.getShaders()[0].psName = "PS_ShadowMap";
}

void CustomScene::setMeshShader(DDEngine::AbstractObject& object)
{
	object.getShaders()[0].psName = "PS_Mesh";
}
