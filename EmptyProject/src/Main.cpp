#include "CustomScene.h"

#if defined(DEBUG)
	#define CONFIG_PATH "../../../data/config.cfg"
#else
	#define CONFIG_PATH "config.cfg"
#endif

using namespace DDEngine;

/*
 Main method, entry point of the application
*/
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int	nCmdShow) {

	/*
	 Create our scene object.
	*/
	CustomScene scene(CONFIG_PATH);

	/*
	 Use bootstrap function to initialize all components of DDEngine
	 and start render loop.
	*/
	return Application::bootstrap(hInstance, scene);
}