
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------

cbuffer Matrices : register(b0)
{
	matrix world;
	matrix invTransWorld;
	matrix view;
	matrix projection;
	matrix depthVP;
	float3 cameraPosition;
}

struct VertexInput 
{
	float3 pos : SV_Position;
	float3 nor : NORMAL;
};

struct VertexOutput 
{
	float4 pos : SV_Position;
	float3 nor : NORMAL;
	float3 tan : TANGENT;
	float3 bitan : BINORMAL;
	float2 tex : TEXCOORD;
	float4 wpos : POSITION0;
	float4 lpos : POSITION1;
};

VertexOutput main( VertexInput input )
{

	float4 pos = float4(input.pos, 1);

	VertexOutput output;
	
	output.pos = mul(pos, world);
	output.pos = mul(output.pos, view);
	output.pos = mul(output.pos, projection);

	output.tex = pos.xy * 5;
	output.nor = normalize(mul(input.nor, world));
	output.tan = normalize(mul(float3(0, 0, 0), world));

	float3 bitan = cross(input.nor, float3(0, 0, 0));
	output.bitan = normalize(mul(normalize(bitan), world));

	output.wpos = mul(pos, world);
	output.lpos = mul(pos, depthVP);

	return output;
}