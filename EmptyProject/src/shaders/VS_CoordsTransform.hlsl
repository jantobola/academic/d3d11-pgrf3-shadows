
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------

static const float PI = 3.14159265f;

cbuffer Matrices : register(b0)
{
	matrix world;
	matrix invTransWorld;
	matrix view;
	matrix projection;
	float3 cameraPosition;
}

cbuffer Props : register(b1)
{
	int objectIndex;
	int objectsNumber;
}

struct VertexInput 
{
	float3 pos : SV_Position;
	float3 nor : NORMAL;
};

struct VertexOutput 
{
	float4 pos : SV_Position;
	float3 nor : NORMAL;
	float3 tan : TANGENT;
	float3 bitan : BINORMAL;
	float2 tex : TEXCOORD;
	float4 wpos : POSITION;
};

// -------------------------------------------------------------------------------------

// Numerical differentiation step
static float h = 0.01f;

struct Vert
{
	float4 position;
	float3 normal;
	float3 tangent;
};

Vert safeVert(float4 position)
{
	Vert vert;
	vert.position = position;
	vert.normal = float3(1, 1, 1);
	vert.tangent = float3(0, 0, 0);
	return vert;
}

float4 getWVPPosition(float4 pos)
{
	float4 output = mul(pos, world);
	output = mul(output, view);
	output = mul(output, projection);
	return output;
}

// ---------------- SPHERICAL CALCULATIONS ----------------

float3 convertFromSpherical(float rho, float phi, float theta)
{
	float3 cartesian;

	cartesian.x = rho * sin(phi) * cos(theta);
	cartesian.y = rho * sin(phi) * sin(theta);
	cartesian.z = rho * cos(phi);

	return cartesian;
}

float3 toSf(float3 v)
{
	float3 param;

	float r = 1;
	float s = v.y * PI;
	float t = v.x * 2*PI;

	param.x = r + sin(s*PI);
	param.y = s;
	param.z = -t;

	return convertFromSpherical(param.x, param.y, param.z);
}

// ---------------- SPHERICAL CALCULATIONS ----------------

// ---------------- CYLINDRICAL CALCULATIONS ----------------

float3 convertFromCylindrical(float r, float s, float t)
{
	float3 cylindrical;

	cylindrical.x = r * cos(t);
	cylindrical.y = r * sin(t);
	cylindrical.z = s;

	return cylindrical;
}

float3 toCl(float3 v)
{
	float3 param;

	float r = 0.6;
	float s = v.y;
	float t = v.x * 2*PI;

	param.x = r + sin(4*t) * r * 0.2;
	param.y = s * 11 - 3;
	param.z = t;

	return convertFromCylindrical(param.x, param.y, param.z);
}

// ---------------- CYLINDRICAL CALCULATIONS ----------------

// ---------------- CARTESIAN CALCULATIONS ----------------

float3 toCs(float3 v)
{
	float r = 0.5;
	float3 param;

	param.x = (r * cos(v.x * 2*PI)) + cos(v.y * 15) * r;
	param.y = (r * sin(v.x * 2*PI)) + sin(v.y * 15) * r;
	param.z = v.y*10-3.1f;

	return param;
}

// ---------------- CARTESIAN CALCULATIONS ----------------

Vert toSpherical(float4 pos)
{
	Vert vert;

	float rho;
	float phi;
	float theta;

	float phiM;
	float phiP;
	float thetaM;
	float thetaP;
			
	float3 vecX;
	float3 vecY;

	float3 sphere;

	switch(objectsNumber) {
		case 1:

			rho = 1;
			phi = pos.x * PI;
			theta = pos.y * 2*PI;
			
			sphere = convertFromSpherical(rho, phi, theta);

			// CALCULATE NORMAL
			
			// ----- Numerical derivative -----

			/*
			phiM = (pos.x - h) * PI;
			phiP = (pos.x + h) * PI;
			thetaM = (pos.y - h) * 2*PI;
			thetaP = (pos.y + h) * 2*PI;

			vecX.x = ((rho * sin(phiP) * cos(theta)) - (rho * sin(phiM) * cos(theta))) / (2*h);
			vecX.y = ((rho * sin(phiP) * sin(theta)) - (rho * sin(phiM) * sin(theta))) / (2*h);
			vecX.z = ((rho * cos(phiP)) - (rho * cos(phiM))) / (2*h);

			vecY.x = ((rho * sin(phi) * cos(thetaP)) - (rho * sin(phi) * cos(thetaM))) / (2*h);
			vecY.y = ((rho * sin(phi) * sin(thetaP)) - (rho * sin(phi) * sin(thetaM))) / (2*h);
			vecY.z = 0;
			
			// ----- Analytical derivative -----

			vecX.x = cos(phi) * cos(theta);
			vecX.y = cos(phi) * sin(theta);
			vecX.z = -sin(phi);

			vecY.x = sin(phi) * -sin(theta);
			vecY.y = sin(phi) * cos(theta);
			vecY.z = 0;

			vert.normal = cross(vecX, vecY).xzy;
			*/

			float3 tanY;

			tanY.x = sin(phi) * -sin(theta);
			tanY.y = sin(phi) * cos(theta);
			tanY.z = 0;

			vert.position = float4(sphere.xzy, 1);
			vert.normal = normalize(sphere.xzy);
			vert.tangent = tanY.xzy;

			return vert;

		case 2:

			sphere = toSf(pos.xyz);
			
			vecX = (toSf(float3(pos.x + h, pos.y, pos.z)) - toSf(float3(pos.x - h, pos.y, pos.z))) / (2*h);
			vecY = (toSf(float3(pos.x, pos.y + h, pos.z)) - toSf(float3(pos.x, pos.y - h, pos.z))) / (2*h);

			vert.position = float4(sphere.xzy, 1);
			vert.normal = cross(vecX, vecY).xzy;
			vert.tangent = float3(1, 1, 1);
			return vert;

		default:
			return safeVert(pos);
	}
}

Vert toCylindrical(float4 pos)
{

	Vert vert;

	float r;
	float theta;
			
	float3 cylinder;

	float3 vecX;
	float3 vecY;

	switch(objectsNumber) {
		case 1:

			r = 1;
			theta = pos.x * 2*PI;
			
			cylinder;

			cylinder.x = r * cos(theta);
			cylinder.y = r * sin(theta);
			cylinder.z = pos.y * 2;

			vert.position = float4(cylinder.xzy, 1);

			// CALCULATE NORMAL

			float thetaM = (pos.x - h) * 2*PI;
			float thetaP = (pos.x + h) * 2*PI;

			vecX.x = ((r * cos(thetaP)) - (r * cos(thetaM))) / 2*h;
			vecX.y = ((r * sin(thetaP)) - (r * sin(thetaM))) / 2*h;
			vecX.z = 0;

			vecY.x = 0;
			vecY.y = 0;
			vecY.z = ((pos.y - h) - (pos.y + h)) / 2*h;

			vert.normal = cross(vecY, vecX).xzy;
			vert.tangent = float3(0, 0, 0);
			return vert;

		case 2:

			cylinder = toCl(pos.xyz);

			vecX = (toCl(float3(pos.x + h, pos.y, pos.z)) - toCl(float3(pos.x - h, pos.y, pos.z))) / (2*h);
			vecY = (toCl(float3(pos.x, pos.y + h, pos.z)) - toCl(float3(pos.x, pos.y - h, pos.z))) / (2*h);

			vert.position = float4(cylinder.xzy, 1);
			vert.normal = cross(vecX, vecY).xzy;
			vert.tangent = float3(0, 0, 0);
			return vert;

		default:
			return safeVert(pos);
	}
}

Vert toCartesian(float4 pos)
{

	Vert vert;

	float3 seat;
	float p;
	float3 vecX;
	float3 vecY;

	switch(objectsNumber) {
		case 1:

			p = 1.8;
			pos = pos * p;

			seat.x = pos.x;
			seat.y = pos.y;
			seat.z = pow(pos.x - 0.5 * p, 8) - pow(pos.y - 0.5 * p, 8);

			vert.position = float4(seat.xzy, 1);

			// CALCULATE NORMAL

			vecX.x = ((pos.x + h) - (pos.x - h)) / 2*h;
			vecX.y = 0;
			vecX.z = ((pow(pos.x + h - 0.5 * p, 8) - pow(pos.y - 0.5 * p, 8)) - (pow(pos.x - h - 0.5 * p, 8) - pow(pos.y - 0.5 * p, 8))) / 2*h;

			vecY.x = 0;
			vecY.y = ((pos.y + h) - (pos.y - h)) / 2*h;
			vecY.z = ((pow(pos.x - 0.5 * p, 8) - pow(pos.y + h - 0.5 * p, 8)) - (pow(pos.x - 0.5 * p, 8) - pow(pos.y - h - 0.5 * p, 8))) / 2*h;

			vert.normal = cross(vecX, vecY).xzy;
			vert.tangent = float3(0, 0, 0);
			return vert;

		case 2:

			seat = toCs(pos.xyz);

			vecX = (toCs(float3(pos.x + h, pos.y, pos.z)) - toCs(float3(pos.x - h, pos.y, pos.z))) / (2*h);
			vecY = (toCs(float3(pos.x, pos.y + h, pos.z)) - toCs(float3(pos.x, pos.y - h, pos.z))) / (2*h);

			vert.position = float4(seat.xzy, 1);
			vert.normal = cross(vecX, vecY).xzy;
			vert.tangent = float3(0, 0, 0);
			return vert;

		default:
			return safeVert(pos);
	}
}

// -------------------------------------------------------------------------------------

VertexOutput main( VertexInput input )
{

	float4 position = float4(input.pos, 1);
	Vert transformedVertex;
	VertexOutput output;

	switch (objectIndex) {

		case 0:
			transformedVertex = toSpherical(position);
			break;

		case 1:
			transformedVertex = toCylindrical(position);
			break;

		case 2:
			transformedVertex = toCartesian(position);
			break;

		default:
			transformedVertex = safeVert(position);
			break;

	}

	output.pos = getWVPPosition(transformedVertex.position);
	output.nor = normalize(mul(normalize(transformedVertex.normal), world));
	output.tan = normalize(mul(normalize(transformedVertex.tangent), world));

	float3 bitan = cross(transformedVertex.normal, transformedVertex.tangent);

	output.bitan = normalize(mul(normalize(bitan), world));
	output.tex = position.yx;
	output.wpos = mul(transformedVertex.position, world);

	return output;
}