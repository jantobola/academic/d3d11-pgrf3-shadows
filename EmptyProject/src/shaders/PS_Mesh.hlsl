//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

#include <lib/ILight.h>
#include <lib/IMaterial.h>

SamplerState sampl : register (s0);
SamplerState pointSampl : register (s1);
Texture2D<float> shadowMap : register (t2);

cbuffer Matrices : register(b0)
{
	matrix world;
	matrix invTransWorld;
	matrix view;
	matrix projection;
	matrix depthVP;
	float3 cameraPosition;
};

struct PixelInput
{
	float4 pos : SV_Position;
	float3 nor : NORMAL;
	float3 tan : TANGENT;
	float3 bitan : BINORMAL;
	float2 tex : TEXCOORD;
	float4 wpos : POSITION0;
	float4 lpos : POSITION1;
};

float4 main(PixelInput input) : SV_Target
{

	float4 materialColor = float4(0.8, 0.8, 0.8, 1);
	float4 materialSpecular = float4(1, 1, 1, 1);
	float3 normal = input.nor;

	if(enableDNSA.x == 1){
		materialColor = getMaterial(0, input.tex, sampl);
	}

	if(enableDNSA.y == 1){
		float3 tangent = normalize(input.tan);
		float3 bitangent = normalize(input.bitan);
		float3x3 TBN = float3x3(tangent, bitangent, normal);

		normal = mul(getMaterial(1, input.tex, sampl).xyz * 2 - 1, TBN);
	}

	if(enableDNSA.z == 1){
		materialSpecular = getMaterial(2, input.tex, sampl);
	}

	normal = normalize(normal);

	float4 a = ambient(materialColor);
	float4 d = diffuse(materialColor, input.wpos.xyz, normal);
	float4 s = specular(materialSpecular, input.wpos.xyz, normal);


	// ------------- SHADOWS --------------

    input.lpos.xyz /= input.lpos.w;

    if( input.lpos.x < -1.0f || input.lpos.x > 1.0f ||
        input.lpos.y < -1.0f || input.lpos.y > 1.0f ||
        input.lpos.z < 0.0f  || input.lpos.z > 1.0f ) 
    {

    	float spot = (lightType == 1) ? 1 : spotlight(input.wpos.xyz, spotAngle, spotOuterAngle);
		return addLight(a, d, s, spot * attenuation(input.wpos.xyz, lightAttenuation / 100000));
    }
 
    input.lpos.x = input.lpos.x / 2 + 0.5;
    input.lpos.y = 1 - (input.lpos.y / 2 + 0.5);

    float shadowMapDepth = shadowMap.Sample(pointSampl, input.lpos.xy).r;
    float bias = 0.00001;

    if ( shadowMapDepth < input.lpos.z - bias) return a;

	// ------------- SHADOWS --------------


	float spot = (lightType == 1) ? 1 : spotlight(input.wpos.xyz, spotAngle, spotOuterAngle);
	return addLight(a, d, s, spot * attenuation(input.wpos.xyz, lightAttenuation / 100000));
}