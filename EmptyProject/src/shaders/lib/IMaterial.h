//--------------------------------------------------------------------------------------
// Material interface
// - works with ILight interface
//--------------------------------------------------------------------------------------

Texture2D<float4> material[11] : register (t100);
/**	Array of different texture types.
 *	This array uses following indexing:
 *
 *	0 - Diffuse texture
 *	
 *	1 - Normal map
 *
 *	2 - Specular map
 *
 *	3 - Ambient map
 *
 *	4 - Emissive texture
 *
 *	5 - Height map
 *
 *	6 - Shininess map - The glossiness is in fact the exponent of the specular
 *		(phong) lighting equation.
 *
 *	7 - Opacity map - The texture defines per-pixel opacity.
 *
 *	8 - Displacement map
 *
 *	9 - Lightmap texture - Both 'Lightmaps' and dedicated 'ambient occlusion maps' are
 *		covered by this material property. The texture contains a
 *		scaling value for the final color value of a pixel. Its
 *		intensity is not affected by incoming light.
 *
 *	10 - Reflection texture - Contains the color of a perfect mirror reflection.
 *		 Rarely used, almost never for real-time applications.
*/

/**
 *	Swizzles values. Swizzling is described in swizzling descriptor.
 *	ILight cbuffer is needed
*/
float4 swizzleValues(float4 val, int typeIndex)
{
	int4 coord = swizzlingDescriptor[typeIndex];
	if(coord.x == 1 && coord.y == 2 && coord.z == 3 && coord.w == 4)
		return val;
	float res[4];

	res[abs(coord.x) - 1] = (coord.x > 0) ? val.x : (1 - val.x);
	res[abs(coord.y) - 1] = (coord.y > 0) ? val.y : (1 - val.y);
	res[abs(coord.z) - 1] = (coord.z > 0) ? val.z : (1 - val.z);
	res[abs(coord.w) - 1] = (coord.z > 0) ? val.w : (1 - val.w);

	return float4(res[0], res[1], res[2], res[3]);
}

/**
 *	Returns a color of texel from texture on a given index.
 *	Returned values are in order described in swizzling descriptor.
*/
float4 getMaterial(int index, float2 coords, SamplerState sampl)
{
	return swizzleValues(material[index].Sample(sampl, coords), index);
}