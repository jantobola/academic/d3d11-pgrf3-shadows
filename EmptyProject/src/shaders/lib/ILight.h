//--------------------------------------------------------------------------------------
// Light interface
//--------------------------------------------------------------------------------------

cbuffer LightBuffer : register(b5)
{
	float3 lightDirection;
	float3 lightPosition;
	float3 eyePosition;

	float4 ambientColor;
	float4 diffuseColor;
	float4 specularColor;

	int4 enableDNSA;
	int4 enableEHSO;
	int3 enableDLR;

	float shininess;

	// maybe move to another cbuffer
	int4 swizzlingDescriptor[11];

	float lightAttenuation;
	float spotAngle;
	float spotOuterAngle;
	
	int lightType;
}

float4 ambient(float4 materialAmbient) {
	return ambientColor * materialAmbient;
}

float4 diffuse(float4 materialDiffuse, float3 fragmentPosition, float3 normal)
{
	float3 norm = normalize(normal);
	float3 dir = normalize(lightPosition - fragmentPosition);

	float dotLight = dot(norm, dir);
	return saturate(diffuseColor * dotLight * materialDiffuse);
}

float4 specular(float4 materialSpecular, float3 fragmentPosition, float3 normal)
{
	float3 norm = normalize(normal);
	float3 surfaceToLight = lightPosition - fragmentPosition;
	float3 dir = normalize(-surfaceToLight);
	float3 view = normalize(eyePosition - fragmentPosition); 
	
	//Phong
	float3 reflectionVector = normalize(reflect(dir, normal));

	//Blinn-Phong
	//float3 reflectionVector = normalize(dir + view);

	//Phong
	float dotP = max(0, dot(reflectionVector, view));

	//Blinn-Phong
	//float dotP = max(0, dot(norm, reflectionVector));

	return saturate(specularColor * materialSpecular * pow(dotP, shininess));
}

float attenuation(float3 fragmentPosition, float attenuation){
	return 1.0 / (1.0 + attenuation * pow(length(lightPosition - fragmentPosition), 2));
}

float4 addLight(float4 ambient, float4 diffuse, float4 specular, float attenuation) {
	return ambient + attenuation * saturate(diffuse + specular);
}

float spotlight(float3 fragmentPosition, float angle, float outerAngle) {
	float lightAngle = degrees(acos(dot(normalize(lightPosition - fragmentPosition), normalize(lightDirection))));
	
	if(lightAngle < angle)
		return 1;
	if(lightAngle > outerAngle)
		return 0;

	return lerp(1, 0, (lightAngle - angle) / (outerAngle - angle));
}