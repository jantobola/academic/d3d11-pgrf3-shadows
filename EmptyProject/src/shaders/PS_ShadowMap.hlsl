//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

#include <lib/ILight.h>
#include <lib/IMaterial.h>

SamplerState sampl : register (s0);

cbuffer Matrices : register(b0)
{
	matrix world;
	matrix invTransWorld;
	matrix view;
	matrix projection;
}

struct PixelInput
{
	float4 pos : SV_Position;
	float3 nor : NORMAL;
	float3 tan : TANGENT;
	float3 bitan : BINORMAL;
	float2 tex : TEXCOORD;
	float4 wpos : POSITION0;
	float4 lpos : POSITION1;
};

float4 main(PixelInput input) : SV_Target
{
	return float4(1, 1, 1, 1);
}