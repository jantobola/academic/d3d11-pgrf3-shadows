#pragma once

#include "CBs.h"
#include <DDEngine.h>
#include <AntTweakBar/AntTweakBar.h>

class CustomScene : public DDEngine::DDEComponent {

	private:

		// Constant buffers
		Matrices cb_matrices;
		Light cb_light;

		// Objects
		unsigned int gridSizeX = 1;
		unsigned int gridSizeY = 1;
		bool lightAnimation = true;

		DDEngine::Grid ground;
		DDEngine::ModelObject jeep;
		DDEngine::ModelObject barrel;
		DDEngine::CubeObject light;

		// Render textures

		DDEngine::RenderToTexture shadowMap;
		ID3D11SamplerState* pointSampler;

		// GUI
		TwBar* switcher;
		void initGUI();

		// Handle methods
		void handleConfig(const std::vector<std::string>& tokens);
		void handleCommand(const std::vector<std::string>& tokens);

		void storeDepthMatrix(DDEngine::AbstractObject& object);
		void setShadowMapShader(DDEngine::AbstractObject& object);
		void setMeshShader(DDEngine::AbstractObject& object);

	public:

		CustomScene(std::string configPath) : DDEngine::DDEComponent(configPath) { }
		~CustomScene() { }

		void create() override;
		void render() override;
};