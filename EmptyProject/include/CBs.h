#pragma once

#include <DirectXMath.h>
#include <ConstantBuffers.h>

struct Matrices : public DDEngine::CB::WorldViewProjection
{
	DirectX::XMFLOAT4X4 depthVP;
	DirectX::XMFLOAT3 cameraPosition;
	float pad;
};

struct Light : public DDEngine::CB::Light
{
	Light() : DDEngine::CB::Light() { }

	int lightType = 1;
};